import { GraduationModule } from './graduation.module';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { TestBed, getTestBed } from '@angular/core/testing';


describe('GraduationModule', () => {
  let graduationModule: GraduationModule;

  beforeAll(() => {
    return TestBed.configureTestingModule({
      imports: [
        TranslateModule
      ]
    });
  });

  beforeEach(() => {
    const injector = getTestBed();
    const translateServiceInstance = injector.get(TranslateService);
    graduationModule = new GraduationModule(translateServiceInstance);
  });

  it('should create an instance', () => {
    expect(graduationModule).toBeTruthy();
  });
});
