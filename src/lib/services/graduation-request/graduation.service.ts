import {Injectable} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {HttpClient} from '@angular/common/http';
import {GraduationRequestData} from './../../graduation';
import {GraduationRequestStep} from './../../graduation-request-step';
import {
  GraduationRequestFormComponent
} from '../../components/graduation-root/graduation-apply/graduation-request/steps/graduation-request-form/graduation-request-form.component';
import {
  GraduationRequirementsCheckComponent
} from '../../components/graduation-root/graduation-apply/graduation-request/steps/graduation-requirements-check/graduation-requirements-check.component';
import {
  GraduationDocumentSubmissionComponent
} from '../../components/graduation-root/graduation-apply/graduation-request/steps/graduation-document-submission/graduation-document-submission.component';
import {
  GraduationCeremonyComponent
} from '../../components/graduation-root/graduation-apply/graduation-request/steps/graduation-ceremony/graduation-ceremony.component';
import {asyncMemoize, ConfigurationService} from '@universis/common';

/**
 *
 * Handles data between api and the students app regarding the graduation process
 *
 */

@Injectable({
  providedIn: 'root'
})
export class GraduationService {

  constructor(
    private _context: AngularDataContext,
    private _configuration: ConfigurationService,
    private _httpClient: HttpClient
  ) {
  }

  /**
   *
   * Decides if there is an available graduation event
   *
   * @param graduationEvent The graduation event as fetched from the server
   *
   */
  graduationPeriodExist(graduationEvent) {
    return graduationEvent
      && !!graduationEvent.validFrom
      && !!graduationEvent.validThrough;
  }

  /**
   *
   * Decides the state of the document submission
   *
   */
  getDocumentsSubmissionStatus(attachmentsStatus): string {

    const status = 'unavailable';


    if (!attachmentsStatus || attachmentsStatus.length === 0) {
      return 'unavailable';
    } else if (attachmentsStatus.every(attachment => attachment.status === 'completed')) {


      return 'completed';
    } else {
      return 'pending';
    }
  }

  /**
   *
   * Decides the graduation request action (document) status
   *
   * @param requestAction The graduation request action object
   *
   */
  getRequestActionStatus(requestAction): string {
    if (requestAction && requestAction.length > 0) {
      return 'completed';
    } else {
      return 'pending';
    }
  }

  /**
   *
   * Decides the status of the graduation requirements check
   *
   * @param graduationRules The graduation rules response from the api
   *
   */
  getGraduationRequirementsCheckStatus(graduationRules) {
    if (!graduationRules.finalResult) {
      return 'unavailable';
    } else if (graduationRules.finalResult.success) {
      return 'completed';
    } else {
      return 'pending';
    }

  }

  /**
   *
   * Matches a graduation request action with a graduation request action type.
   *
   * @param requestActionTypes The list of available action types
   * @param requestActions The list of the request actions (documents)
   *
   */
  getDocuments(graduationEvent: any, graduationRequest: any) {

    let requestActionTypes = graduationEvent && graduationEvent.attachmentTypes ? graduationEvent.attachmentTypes : [];
    let requestActions = graduationRequest && graduationRequest.attachments ? graduationRequest.attachments : [];

    // exclude physical attachments from step status control
    requestActionTypes = requestActionTypes.filter((actionType) =>
      !actionType.attachmentType || !actionType.attachmentType.physical
    );

    requestActions = requestActions.filter((actionType) =>
      !actionType.physical === true
    );

    const filteredRequestActionTypes = requestActionTypes.filter((actionType) =>
      !!actionType.attachmentType && actionType.attachmentType.name && !!actionType.attachmentType.alternateName
    );
    if (filteredRequestActionTypes.length !== requestActionTypes.length) {
      console.warn('Graduation action type(s) without name or alternateName found and removed');
    }

    // check if the attachments  files are the required ones
    let requestDocumentsStatus = filteredRequestActionTypes.map((requestActionType) => {
      const selectedRequestAction = requestActions.filter(
        (requestAction) => requestAction.attachmentType.id === requestActionType.attachmentType.id
      );
      return {
        alternateName: requestActionType.attachmentType.alternateName,
        name: requestActionType.attachmentType.name,
        description: requestActionType.attachmentType.description,
        document: requestActionType,
        status: this.getRequestActionStatus(selectedRequestAction),
        requestActionType
      };
    });

    requestDocumentsStatus = requestDocumentsStatus.filter(
      (requestDocument) => requestDocument && requestDocument.alternateName
    );
    return requestDocumentsStatus;
  }

  /**
   *
   * Creates a bundle with information regarding the graduation process
   * Returns the overview of the graduation process
   *
   */
  async getGraduationRequestData(): Promise<GraduationRequestData> {
    // available statuses: 'unavailable', 'available', 'submitted', 'pending'

    let student;
    let availableGraduationEvent;
    let graduationRequest;
    let graduationRules;
    let courseTypes;

    // get data for request and steps from API
    try {
      [student, availableGraduationEvent, graduationRequest, graduationRules, courseTypes] = await Promise.all([
        this.getStudent('me'),
        this.getAvailableGraduationEvent(),
        this.getGraduationRequest('me'),
        this.getGraduationRules('me'),
        this.getCourseTypes()
      ]);
    } catch (err) {
      console.error(err);
      student = availableGraduationEvent = graduationRequest = graduationRules = courseTypes = undefined;
    }

    // if the request is potential and graduationEvent is not available student can apply to new graduationEvent
    if (graduationRequest && (graduationRequest.actionStatus.alternateName === 'PotentialActionStatus' ||
      graduationRequest.actionStatus.alternateName === 'CompletedActionStatus') &&
      ((availableGraduationEvent && graduationRequest.graduationEvent.id !== availableGraduationEvent.id) || !availableGraduationEvent )) {
      if (availableGraduationEvent && graduationRequest.actionStatus.alternateName === 'CompletedActionStatus' &&
        student.studentStatus.alternateName === 'declared') {
        // if the graduationEvent date is older, then student can apply to new graduationEvent
        if (availableGraduationEvent.startDate <  new Date()) {
          graduationRequest = undefined;
        }
      } else {
        graduationRequest = undefined;
      }
    }

    // When there is not graduation event, the available graduation event is being used (misses attachment data)
    // graduationEvent used on steps only
    const graduationEvent = graduationRequest ? graduationRequest.graduationEvent : availableGraduationEvent;

    // get data for all steps
    // 1st step
    const graduationRequestStatus = this.getGraduationRequestStepStatus(graduationRequest);
    const graduationRequestStep = this.getStep(
      'graduationRequest',
      'GraduationRequest',
      graduationRequestStatus,
      false,
      true,
      {
        graduationRequest,
        graduationEvent
      }
    );

    // 2nd step
    const graduationRequirementsStatus = this.getGraduationRequirementsStepStatus(!!graduationRequest, graduationRules);
    const graduationRequirementsCheck = this.getStep(
      'graduationRequirementsCheck',
      'RequirementsCheck',
      graduationRequirementsStatus,
      false,
      true,
      {
        student,
        graduationInfo: graduationRules,
        courseTypes
      }
    );

    // 3rd step
    const attachmentsStatus = this.getDocuments(graduationEvent, graduationRequest);
    const graduationDocumentsSubmissionStatus = this.getDocumentsSubmissionStepStatus(attachmentsStatus, graduationEvent);
    let graduationDocumentsSubmissionStep;
    if (graduationDocumentsSubmissionStatus !== 'unavailable') {
      graduationDocumentsSubmissionStep = this.getStep(
        'graduationDocumentsSubmission',
        'DocumentsSubmission',
        graduationDocumentsSubmissionStatus,
        true,
        true,
        {
          documentsSubmissionStatus: this.getDocumentsSubmissionStatus(attachmentsStatus),
          attachments: attachmentsStatus,
          graduationRequest: graduationRequest
        }
      );
    }

    // 4th step
    const graduationCeremonyStepStatus = this.getGraduationCeremonyStepStatus(
      graduationRequestStatus,
      graduationRequirementsStatus,
      graduationDocumentsSubmissionStatus);
    const graduationCeremonyStep = this.getStep(
      'graduationCeremony',
      'GraduationCeremony',
      graduationCeremonyStepStatus,
      false,
      true,
      {
        certificates: [],
      }
    );

    const data = {
      graduationEvent: availableGraduationEvent,
      graduationRequest: graduationRequest,
      steps: [
        graduationRequestStep,
        graduationRequirementsCheck,
        graduationCeremonyStep
      ]
    };

    // add third step with documents upload
    if (graduationDocumentsSubmissionStatus !== 'unavailable') {
      data.steps.splice(2, 0, graduationDocumentsSubmissionStep);
    }
    return data;
  }

  /**
   *
   * Gets information regarding the student
   *
   */
  async getStudent(student: any) {
    return this._context.model(`students/${student}`)
      .asQueryable()
      .expand('user', 'department', 'studyProgram', 'inscriptionMode', 'person($expand=gender)')
      .getItem().then(result => {
        if (typeof result === 'undefined') {
          return Promise.reject('Student data cannot be found');
        }
        return this._context.model('LocalDepartments')
          .asQueryable()
          .where('id').equal(result.department.id)
          .expand('organization($expand=instituteConfiguration,registrationPeriods)')
          .getItem().then(department => {
            if (typeof department === 'undefined') {
              return Promise.reject('Student department cannot be found');
            }
            result.department = department;
            sessionStorage.setItem('student', JSON.stringify(result));
            return Promise.resolve(result);
          });
      });
  }

  /**
   *
   * Get the student's graduation request
   *
   */
  async getGraduationRequest(student: any) {
    // const currentDate = new Date();
    // currentDate.setHours(0, 0, 0, 0);

    const graduationRequest = await this._context.model(`students/${student}/graduationRequests`)
      .asQueryable()
      // .and('graduationEvent/validThrough').lowerOrEqual(currentDate)
      .where
      ('actionStatus/alternateName').notEqual('CancelledActionStatus').and
      ('actionStatus/alternateName').notEqual('FailedActionStatus')
      .expand('graduationEvent($expand=graduationPeriod,graduationYear,location,attachmentTypes($expand=attachmentType),eventStatus),attachments($expand=attachmentType)')
      .orderByDescending('dateCreated')
      .take(1)
      .getItem();

    return graduationRequest;
  }

  /**
   * Uploads a graduation attachment
   */
  async uploadGraduationRequestAttachment(data) {
    const formData: FormData = new FormData();
    formData.append('file', data.file, data.file.name);
    formData.append('file[attachmentType]', data.attachmentType);

    // get context service headers
    const headers = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`students/me/requests/${data.graduationRequestId}/attachments/add`);

    await this._httpClient.post(postUrl, formData, {
      headers: headers
    }).toPromise().then(result => {
      return this.getGraduationRequestData();
    });
  }

  /**
   *
   * Remove a graduation request document
   *
   * @param requestId The id of the request
   * @param attachmentId  The id of the attachment
   *
   */
  async removeGraduationRequestAttachment(data) {
    const requestId = data.requestId;
    const attachmentId = data.attachmentId;
    const attachmentTypeAlternateName = data.attachmentTypeAlternateName;
    try {
      await this._context.model(`students/me/requests/${requestId}/attachments/${attachmentId}/remove`).save({});
      return await this.getGraduationRequestData();
    } catch (err) {
      const newStatus = await this.getGraduationRequestData();
      if (!newStatus.steps[2].data.errors) {
        newStatus.steps[2].data.errors = {};
      }
      newStatus.steps[2].data.errors.Remove = {};
      newStatus.steps[2].data.errors.Remove[attachmentTypeAlternateName] = err;
      return newStatus;
    }
  }

  /**
   *
   * Downloads the a graduation request attachment
   *
   * @param data The data that contains information about the file
   *
   */
  async downloadGraduationRequestAttachment(data): Promise<any> {
    const attachmentAlternateName = data.attachmentAlternateName;
    const attachmentTypeAlternateName = data.attachmentTypeAlternateName;

    try {
      const headers = this._context.getService().getHeaders();
      const base = this._context.getBase();
      const file = await this._httpClient.get(`${base}content/private/${attachmentAlternateName}`, {
        responseType: 'blob',
        headers
      }).toPromise();

      this.triggerFileDownloadPopup(file, attachmentAlternateName);
      return await this.getGraduationRequestData();
    } catch (err) {
      const newStatus = await this.getGraduationRequestData();

      if (!newStatus.steps[2].data.errors) {
        newStatus.steps[2].data.errors = {};
      }
      newStatus.steps[2].data.errors.Download = {};
      newStatus.steps[2].data.errors.Download[attachmentTypeAlternateName] = err;

      return newStatus;
    }
  }

  /**
   *
   * Creates a graduation request for a student
   *
   * @param graduationEvent The graduation event for which the student is applying
   * @param name The special request from the student
   *
   */
  async setGraduationRequest(graduationEventId: number, graduationForm) {
    try {
      await this._context.model(`graduationRequestActions`).save({
        graduationEvent: {
          id: graduationEventId
        },
        name: graduationForm.name,
        description: graduationForm.specialRequest
      });

      const status = await this.getGraduationRequestData();
      return status;
    } catch (err) {
      const status = await this.getGraduationRequestData();
      if (!status.steps[0].data) {
        status.steps[0].data = {};
      }

      if (!status.steps[0].data.errors) {
        status.steps[0].data.errors = {};
      }

      status.steps[0].data.errors.graduationRequest = err.error && err.error.code
        ? err.error.code
        : err;
      return status;
    }
  }

  /**
   *
   * Fetches the available event status
   *
   */
  async getAvailableGraduationEvent() {
    return this._context.model('students/me/availableGraduationEvents')
      .asQueryable()
      .expand('graduationYear,graduationPeriod,location')
      .getItem();
  }

  /**
   *
   * Gets the graduation rules for a student
   *
   */
  getGraduationRules(student: any): any {
    return this._context.model(`students/${student}/graduationRules`)
      .asQueryable()
      .expand('validationResult')
      .take(-1)
      .getItems();
  }

  /**
   *
   * Resolves alternateNames to angular components
   *
   * @param alternateName The alternate name of the component
   *
   */
  getComponent(alternateName: string): GraduationRequestStep {
    switch (alternateName) {
      case 'graduationRequest':
        return new GraduationRequestStep(GraduationRequestFormComponent, {});
      case 'graduationRequirementsCheck':
        return new GraduationRequestStep(GraduationRequirementsCheckComponent, {});
      case 'graduationDocumentsSubmission':
        return new GraduationRequestStep(GraduationDocumentSubmissionComponent, {});
      case 'graduationCeremony':
        return new GraduationRequestStep(GraduationCeremonyComponent, {});
    }
  }

  /**
   *
   * triggers a file download popup at the browser
   *
   * @param blob The file as received from the server
   */
  triggerFileDownloadPopup(blob: Blob, fileName: string) {
    const objectUrl = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = objectUrl;
    const extension = 'txt'; // TODO: change the extension
    a.download = `${fileName}-.${extension}`;
    a.click();
    window.URL.revokeObjectURL(objectUrl);
    a.remove();
  }

  getStep(alternateName: string, title: string, status: string, required: boolean, enabled: boolean, data: any) {
    if (!data) {
      data = {};
    }

    if (!data.errors) {
      data.errors = {};
    }

    return {
      title: `UniversisGraduationModule.${title}.Title`,
      alternateName: alternateName,
      status,
      required,
      enabled,
      component: this.getComponent(alternateName),
      data
    };
  }

  // The following functions are ad-hoc implementation and should be encapsulated
  // inside a GraduationRequestStep data   structure. An abstraction over them is a
  // nice to have feature

  /**
   * 1st step
   * The graduation request is available only if there is not
   * a not failed or canceled request
   *
   * @param graduationPeriodExist A flag that states whether a graduation period exists
   * @param graduationRequest The graduation request of the student
   *
   */
  getGraduationRequestStepStatus(graduationRequest): string {
    if (graduationRequest) {
      return 'completed';
    } else {
      return 'failed';
    }
  }

  /**
   * 2nd step
   * The graduationRequirementsCheck step is available only if there is a graduation request.
   * It is unavailable if there is not a graduation request.
   * It is failed if there is at least a failed step.
   *
   * @param graduationRequestExists A flag that states whether a graduation request exists
   * @param graduationRules The graduation rules list
   *
   */
  getGraduationRequirementsStepStatus(graduationRequestExists: boolean, graduationRules) {
    if (!graduationRequestExists || !graduationRules.finalResult) {
      return 'unavailable';
    } else if (!graduationRules.finalResult.success) {
      return 'failed';
    } else if (graduationRules.finalResult.success) {
      return 'completed';
    } else {
      return 'unavailable';
    }
  }

  /**
   * 3rd step
   * The documents submission step is available only if the requirements check have passed
   * It is completed when the documents are accepted
   * It is failed if a document is rejected
   *
   * @param attachmentsStatus The status of the attachments
   *
   */
  getDocumentsSubmissionStepStatus(attachmentsStatus, graduationEvent: any) {
    if (!graduationEvent || !graduationEvent.attachmentTypes || graduationEvent.attachmentTypes.length === 0) {
      return 'unavailable';
    } else if (attachmentsStatus && attachmentsStatus.length > 0 &&
      graduationEvent && graduationEvent.attachmentTypes && graduationEvent.attachmentTypes.length > 0) {
      const results = attachmentsStatus.filter(
        (requestAction) => requestAction.status === 'completed'
      );

      // exclude physical attachments from step status control
      let requestActionTypes = graduationEvent.attachmentTypes ? graduationEvent.attachmentTypes : [];
      requestActionTypes = requestActionTypes.filter((actionType) =>
        !actionType.attachmentType.physical
      );

      if (results && results.length > 0 && results.length === requestActionTypes.length) {
        return 'completed';
      }
      return 'failed';
    } else {
      return 'unavailable';
    }
  }

  getGraduationCeremonyStepStatus(status1, status2, status3) {
    if (status1 === 'completed' && status2 === 'completed' && (status3 === 'completed' || status3 === 'unavailable')) {
      return 'completed';
    } else {
      return 'failed';
    }
  }

  /**
   *
   * Gets information regarding the course types
   *
   */
  @asyncMemoize()
  getCourseTypes(): any {
    return this._context.model('courseTypes')
      .take(-1)
      .getItems();
  }

  /**
   *
   * Creates a graduation request for a student
   *
   * @param graduationEvent The graduation event for which the student is applying
   * @param name The special request from the student
   *
   */
  async setGraduationRequestActiveStatus(graduationRequestData) {
    try {
      await this._context.model(`graduationRequestActions`).save({
        additionalType: 'GraduationRequestAction',
        graduationEvent: {
          id: graduationRequestData.graduationEvent.id
        },
        actionStatus: {
          additionalType: 'ActionStatusType',
          alternateName: 'ActiveActionStatus',
          name: 'Active',
          id: 4
        },
        student: graduationRequestData.graduationRequest.student,
        owner: graduationRequestData.graduationRequest.owner,
        id: graduationRequestData.graduationRequest.id
      });
    } catch (err) {
      return null;
    }
  }

  async getStudentRequestConfigurations() {
    // this._configuration.currentLocale
    return this._context.model('StudentRequestConfigurations')
      .asQueryable()
      .where('inLanguage')
      .equal('el')
      .expand('category')
      .getItems();
  }
}
