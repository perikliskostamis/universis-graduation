import { el } from './graduation.el';
import { en } from './graduation.en';

export const GRADUATION_LOCALES = {
  el,
  en
};
