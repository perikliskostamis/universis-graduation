import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduationPrerequisitesComponent } from './graduation-prerequisites.component';

describe('GraduationRulesComponent', () => {
  let component: GraduationPrerequisitesComponent;
  let fixture: ComponentFixture<GraduationPrerequisitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduationPrerequisitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduationPrerequisitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
