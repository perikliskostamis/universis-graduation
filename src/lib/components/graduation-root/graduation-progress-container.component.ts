import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'universis-graduation-progress-container',
  template: `<universis-graduation-progress [student]="'me'"></universis-graduation-progress>`
})
export class GraduationProgressContainerComponent {

}
