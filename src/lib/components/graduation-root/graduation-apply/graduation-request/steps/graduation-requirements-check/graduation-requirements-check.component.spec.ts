import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduationRequirementsCheckComponent } from './graduation-requirements-check.component';

describe('GraduationRequirementsCheckComponent', () => {
  let component: GraduationRequirementsCheckComponent;
  let fixture: ComponentFixture<GraduationRequirementsCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduationRequirementsCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduationRequirementsCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
