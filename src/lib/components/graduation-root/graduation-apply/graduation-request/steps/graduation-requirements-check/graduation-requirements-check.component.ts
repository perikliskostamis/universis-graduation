import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Subscription } from 'rxjs/internal/Subscription';
import { TranslateService } from '@ngx-translate/core';
import { GraduationWizardTabComponent } from '../../../graduation-wizard-tab/graduation-wizard-tab.component';

@Component({
  selector: 'universis-graduation-requirements-check',
  templateUrl: './graduation-requirements-check.component.html'
})
export class GraduationRequirementsCheckComponent extends GraduationWizardTabComponent implements OnInit, OnDestroy {

  public courseTypes: any;
  public graduationInfo: any;
  public studentGuide: any;
  public studentSpecialty: any;
  public currentStep: any;

  /**
   *
   * A message for the status of the overall process
   *
   */
  public statusMessage: string | undefined;


  constructor(
    private _translateService: TranslateService
  ) {
    super();
    this.alternateName = 'graduationRequirementsCheck';
   }


  ngOnInit() {
    this.graduationRequestStatusSubscription = this.graduationRequestStatusObservable$.subscribe((graduationStatus) => {
      this.graduationRequestData = graduationStatus;

      this.currentStep = this.getStep(this.graduationRequestData);

      if (this.currentStep && this.currentStep.data) {
        this.courseTypes = this.currentStep.data.courseTypes;
        this.graduationInfo = this.currentStep.data.graduationInfo;
        this.studentGuide = this.currentStep.data.student.studyProgram.name;
        this.studentSpecialty = this.currentStep.data.student.specialty;

        if (this.currentStep.status) {
          this.statusMessage = this._translateService.instant(
            `UniversisGraduationModule.RequirementsCheck.Status.${this.currentStep.status}`
          );
        } else {
          this.statusMessage = undefined;
        }
      }
    });
  }

  ngOnDestroy(): void {
    if (this.graduationRequestStatusSubscription) {
      this.graduationRequestStatusSubscription.unsubscribe();
    }
  }
}
