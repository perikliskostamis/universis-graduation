import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduationCeremonyComponent } from './graduation-ceremony.component';

describe('GraduationCeremonyComponent', () => {
  let component: GraduationCeremonyComponent;
  let fixture: ComponentFixture<GraduationCeremonyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduationCeremonyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduationCeremonyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
