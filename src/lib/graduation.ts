/**
 *
 * Type definitions for the graduation module
 *
 */

import { GraduationRequestStep } from './graduation-request-step';


/**
 *
 * Possible values for graduation item statuses
 * Is used for graduation request steps, graduation rules and document submissions
 * @enum {string}
 * @readonly
 *
 * @property {string} Available The item is accessible
 * @property {string} Pending The item is be processed by other entity than the user
 * @property {string} Completed The item is completed successfully
 * @property {string} Failed The process that the item resemble is failed or completed with errors
 * @property {string} Unavailable The item is not available to the user yet
 *
 */
export enum GraduationItemStatus {
  Available = 'available',
  Pending = 'pending',
  Completed = 'completed',
  Failed = 'failed',
  Unavailable = 'unavailable',
}

/**
 *
 * Possible values for a graduation rule
 * @enum {string}
 * @readonly
 *
 * @property {string} Simple A standalone requirement rule
 * @property {string} LogicOr Has children rule and they are combined with the 'or' operator
 * @property {string} LogicAnd Has children rule and they are combined with the 'and' operator
 *
 */
export enum GraduationRequirementType {
  Simple = 'simple',
  LogicOr = 'or',
  LoginAnd = 'and',
}

/**
 *
 * Graduation requirements a simple or nested items which resemble rules that the
 * student must fulfil in order to be able to graduate.
 * These rules can be nested and be combined with operators against each other
 *
 * @property {string} title The graduation requirement title
 * @property {string} description The description of th requirement
 * @property {GraduationRequirementType} type Requirements can be simple or used with operators
 * @property {GraduationItemStatus} status The current status of the rule for a given student
 * @property {Array<GraduationRequirement>} [subrules] Child rules for the current rule
 *
 */
export interface GraduationRequirement {
  title: string;
  description: string;
  type: GraduationRequirementType;
  status: GraduationItemStatus;
  subrules?: Array<GraduationRequirement>;
}

/**
 *
 * A step of the graduation request wizard
 *
 * @property {string} title The title of the step
 * @property {string} alternateName The alternate name of the step
 * @property {string} status The status of the step
 * @property {boolean} required If the step it's required then is true
 * @property {boolean} enabled If the step it's available is true
 * @property {GraduationRequestStep} component The component to use for the step
 * @property {any} [data] Step-specific data for each component`
 *
 */
export interface GraduationRequestStepEntry {
  title: string;
  alternateName: string;
  status: string;
  required: boolean;
  enabled: boolean;
  component: GraduationRequestStep;
  data?: any;
}

/**
 *
 * An overview of the graduation request process
 * @property {any} graduationEvent The available selected event for student, if this exists
 * @property {any} graduationRequest The data of the request
 * @property {Array<GraduationRequestStepEntry>} steps All steps for this request
 *
 */
export interface GraduationRequestData {
  graduationEvent: any;
  graduationRequest: any;
  steps: Array<GraduationRequestStepEntry>;
}
